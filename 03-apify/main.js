/*jshint esversion: 8 */

const Apify = require("apify");

Apify.main(async () => {
  const browser = await Apify.launchPuppeteer();

  const page = await browser.newPage();
  await page.goto("http://www.nhl.com/stats/cs");

  await page.waitForSelector("ul.leaders-list li");

  const table = await page.$$("ul.leaders-list li");
  for (const line of table) {
    const name = await page.evaluate(
      (el) => el.textContent,
      await line.$("span:first-child")
    );
    const points = await page.evaluate(
      (el) => el.textContent,
      await line.$("span:last-child")
    );
    console.log({ name, points });
  }

  await browser.close();
});
