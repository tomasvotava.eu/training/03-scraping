"""Super easy, barely an inconvenience
"""

import json
import requests
from bs4 import BeautifulSoup
from helpers import EmptyNode


def main():
    """Download and conquer"""
    with requests.Session() as session:
        response = session.get("https://isport.blesk.cz/nhl-2020-2021-kanadske-bodovani")
        soup = BeautifulSoup(response.text, features="html.parser")
    container = soup.select_one(".table-league.hockey")
    if not container:
        return
    for line in container.select("tr"):
        name = (line.select_one("td:nth-child(2)") or EmptyNode).text.strip()
        if not name:
            continue
        team = (line.select_one("td.logo") or EmptyNode).attrs.get("title")
        points = (line.select_one("td.sport-color") or EmptyNode).text.strip()
        yield {"name": name, "team": team, "points": points}


if __name__ == "__main__":
    with open("output.json", "w", encoding="utf-8") as fid:
        json.dump(list(main()), fid, indent=2)
