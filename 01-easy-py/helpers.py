"""Helpers
"""


from typing import Mapping


class EmptyNode:
    text = ""
    attrs: Mapping[str, str] = {}
