# Easy Sites Scraping - Python

## Install

```console
python -m venv venv
. venv/bin/activate
pip install requests beautifulsoup4
```

## Run

```console
. venv/bin/activate
python main.py
```
