/*jshint esversion: 6 */

const axios = require("axios").default;
const cheerio = require("cheerio");
const fs = require("fs");

axios
  .get("https://isport.blesk.cz/nhl-2020-2021-kanadske-bodovani")
  .then((response) => {
    const $ = cheerio.load(response.data);
    const players = $(".table-league.hockey tr").map((_, el) => {
      const player = {
        name: $("td:nth-child(2)", el).text().trim(),
        team: $("td.logo", el).attr("title"),
        points: $("td.sport-color", el).text().trim(),
      };
      if (player.name) return player;
    });
    fs.writeFileSync("output.json", JSON.stringify([...players], null, 2), {
      encoding: "utf-8",
    });
  });
