# Easy Sites Scraping - NodeJS

## Install

### Using `npm`

```console
npm install
```

### Using `yarn`

```console
yarn install
```

## Run

```console
node index.js
```
