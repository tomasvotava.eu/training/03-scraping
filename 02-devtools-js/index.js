/*jshint esversion: 6 */

const axios = require("axios").default;
const fs = require("fs");

axios
  .get(
    "https://api.nhle.com/stats/rest/cs/leaders/skaters/points?cayenneExp=season=20202021"
  )
  .then((response) => {
    const players = response.data.data.map((el) => {
      return {
        name: el.player.fullName,
        team: el.team.fullName,
        points: el.points,
      };
    });
    fs.writeFileSync("output.json", JSON.stringify(players, null, 2));
  });
