# Internal API Scraping - Python

## Install

```console
python -m venv venv
. venv/bin/activate
pip install requests
```

## Run

```console
. venv/bin/activate
python main.py
```
