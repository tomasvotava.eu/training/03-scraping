"""Very easy, as well
"""

import json
import requests


def main():
    """Do that"""
    with requests.Session() as session:
        response = session.get(
            "https://api.nhle.com/stats/rest/cs/leaders/skaters/points?cayenneExp=season=20202021"
        )
    data = response.json().get("data", [])
    for player in data:
        yield {
            "name": player.get("player", {}).get("fullName"),
            "team": player.get("team", {}).get("fullName"),
            "points": player.get("points"),
        }


if __name__ == "__main__":
    with open("output.json", "w", encoding="utf-8") as fid:
        json.dump(list(main()), fid, indent=2)
